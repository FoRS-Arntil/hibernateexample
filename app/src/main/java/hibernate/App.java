/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class App {
    public static void main(String[] args) {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().build();

        SessionFactory sessionFactory = new MetadataSources(registry)
                            .addAnnotatedClass(Personne.class)
                            .addAnnotatedClass(Adresse.class)
                            .buildMetadata()
                            .buildSessionFactory();

        Personne personne = new Personne("Jean Duchemin", "Super secret");
        personne.ajouterAdresse(new Adresse("Rue des maroniers", "Liège", "Belgique"));
        personne.ajouterAdresse(new Adresse("Place du grognon", "Namur", "Belgique"));

        sessionFactory.inTransaction(session -> {
            session.persist(personne);
        });
        sessionFactory.inSession((session -> {
            session.createSelectionQuery("from Personne", Personne.class).getResultList()
                .forEach(pers -> {
                    System.out.println("Personne:");
                    System.out.println("Id : " + pers.getId());
                    System.out.println("Nom : " + pers.getNom());
                    System.out.println("Secret : " + pers.getSecret());
                    System.out.println("Adresses:");
                    pers.getAdresses().forEach(adresse -> {
                        System.out.println("Id : " + adresse.getId());
                        System.out.println("Rue : " + adresse.getRue());
                        System.out.println("Ville :  " + adresse.getVille());
                        System.out.println("Pays :  " + adresse.getPays());
                    });
                });
        }));

        sessionFactory.close();
    }
}
