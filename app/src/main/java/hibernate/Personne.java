package hibernate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Entity
@Table(name = "Personnes")
public class Personne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    @OneToMany(mappedBy = "personne", cascade = CascadeType.ALL)
    private Set<Adresse> adresses;
    @Transient
    private String secret;

    public Personne () {
        if (this.adresses == null)
            this.adresses = new HashSet<>();
    }

    public Personne(String nom, String secret) {
        this.nom = nom;
        this.secret = secret;
        if (this.adresses == null)
            this.adresses = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getNom() {
        return nom;
    }

    public void ajouterAdresse(Adresse adresse) {
        adresses.add(adresse);
        adresse.setPersonne(this);
    }

    public Set<Adresse> getAdresses() {
        return adresses;
    }
}
